# linguaHub-classroom-app

Welcome to LinguaHub Pro English Language Academy! This web application is designed to provide an experience for administrators, teachers, and students to manage and engage with language learning courses effectively.

## Link: <a href="https://angular-test-project-a4785.web.app"> LinguaHub Pro</a>

## Technologies

- Angular 16
- TypeScript
- CSS
- Angular-Material UI
- RxJS
- Firebase Rest API
- Firebase Realtime Database

## Features

- Admin Dashboard: Manage user roles (4 - new user, 3 - student, 2 - teacher, 1 - admin), create, update, and delete courses, assign students and teachers to courses.
- Teacher Dashboard: Create themes for courses, add comments to courses.
- Student Dashboard: Access courses, view course themes, and add comments.

## Usage

- Admin Dashboard: Log in as an administrator to access the admin dashboard. Here, you can manage user roles, create, update, and delete courses, and assign students and teachers.
- Course Dashboard: Log in as a teacher to access the course dashboard. Here, you can create themes for courses and add comments.
- Course Dashboard: Log in as a student to access the course dashboard. Here, you can access courses, view course themes, and add comments.

# Test

Admin:  
e-mail: *admin@test.com*  
password: 123456

Teacher:  
e-mail: *teacher@test.com*  
password: 123456

Student:
e-mail: *student@test.com*  
password: 123456
<br/>

#### **Pages**

<div style="display: flex; justify-content: start; gap: 2rem">
    <img src="./linguaHub/src/assets/readme-images/home.png" width="400px">
    <img src="./linguaHub/src/assets/readme-images/dashboard-admin.png"width="400px">
</div>
<br/>
<div style="display: flex; justify-content: start; gap: 2rem"> 
    <img src="./linguaHub/src/assets/readme-images/course.png"width="400px">
    <img src="./linguaHub/src/assets/readme-images/lesson.png"width="400px"> 
</div>
<br/>
<div style="display: flex; justify-content: start; gap: 2rem">   
    <img src="./linguaHub/src/assets/readme-images/about2.png"width="400px">  
    <img src="./linguaHub/src/assets/readme-images/dict.png"width="400px"> 
</div>
