export const DICTIONARY_API =
  'https://api.dictionaryapi.dev/api/v2/entries/en/';

export const AUTH_REGISTER_URL =
  'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyB2FW8iCV7jkjaarIm1SzSau6XvfFWpPXs';

export const AUTH_LOGIN_URL = `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyB2FW8iCV7jkjaarIm1SzSau6XvfFWpPXs`;

export const REALTIME_URL =
  'https://angular-test-project-a4785-default-rtdb.europe-west1.firebasedatabase.app';
