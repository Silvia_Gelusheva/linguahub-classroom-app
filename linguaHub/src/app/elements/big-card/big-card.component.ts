import { Component, EventEmitter, Input, Output } from '@angular/core';
@Component({
  selector: 'app-big-card',
  templateUrl: './big-card.component.html',
  styleUrls: ['./big-card.component.css'],
})
export class BigCardComponent {
  @Input() title: string = '';
  @Input() subTitle: string = '';
  @Input() text: string = '';
  @Input() showImage: boolean = false;
  @Input() showButton: boolean = false;
  @Input() showSocialIcons: boolean = false;
  @Input() showHeader: boolean = false;
  @Input() showSubtitle: boolean = false;
  @Input() showText: boolean = false;
  @Input() customClass: string = '';
  @Input() buttonLabel: string = '';
  @Input() image: string = '';
  @Input() applyCustomStyles: boolean = false;
  @Input() customStyle: {} = {};

  @Output() buttonClick = new EventEmitter<void>();

  onButtonClick() {
    this.buttonClick.emit();
  }
}
