import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-small-card',
  templateUrl: './small-card.component.html',
  styleUrls: ['./small-card.component.css'],
})
export class SmallCardComponent {
  @Input() image: string = '';
  @Input() title: string = '';
  @Input() subTitle: string = '';
  @Input() text: string = '';
  @Input() showButton: boolean = false;
  @Input() showHeader: boolean = false;
  @Input() showSubtitle: boolean = false;
  @Input() showText: boolean = false;
  @Input() showImage: boolean = false;
  @Input() customClass: string = '';
  @Input() applyCustomStyles: boolean = false;
  @Input() customStyle: { [key: string]: string } = {};
}
