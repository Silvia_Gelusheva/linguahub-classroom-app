export interface CommentData {
  id?: string;
  text: string;
  addedOn: Date;
  author: string;
  editModeComment?: boolean;
}
