import { CommentData } from './CommentData';

export interface ThemeData {
  id?: string;
  lesson: number;
  title: string;
  text: string;
  homework: string;
  author: string;
  addedOn: Date;
  comments: { [commentId: string]: CommentData };
  editMode?: boolean;
}
