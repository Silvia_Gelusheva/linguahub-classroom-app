export interface UserData {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
  role: number;
  course?: [];
  assignedTo?: string;
}
