import { UserData } from './UserData';

export interface CourseData {
  id: string;
  name: string;
  teacher: string;
  type: string;
  level: number;
  students?: string[];
  startDate?: Date;
  endDate?: Date;
  editMode?: boolean;
}
