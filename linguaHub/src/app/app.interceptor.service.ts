import {
  HttpEventType,
  HttpHandler,
  HttpInterceptor,
  HttpParams,
  HttpRequest,
} from '@angular/common/http';
import { exhaustMap, take } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { UserService } from './user/user.service';

@Injectable()
export class AppInterceptorService implements HttpInterceptor {
  constructor(private userService: UserService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return this.userService.user.pipe(
      take(1),
      exhaustMap((user) => {
        // console.log('User:', user);

        if (!user) {
          // console.log('User is not logged in.');
          return next.handle(req);
        }

        // console.log('User is logged in:', user);

        const token = user.token || '';
        // console.log('Token:', token);

        const modifiedReq = req.clone({
          params: new HttpParams().set('auth', token),
        });

        // console.log('Modified Request:', modifiedReq);

        return next.handle(modifiedReq);
      })
    );
  }
}
