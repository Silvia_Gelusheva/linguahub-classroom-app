import { Component, OnDestroy, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/types/User';
import { UserData } from 'src/app/types/UserData';
import { UserService } from 'src/app/user/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  isLoggedIn: boolean = false;
  private userSubject!: Subscription;
  user: UserData | undefined;
  showDropdownMenu = false;
  aboutDropdown = false;
  programmesDropdown = false;
  aboutDropdownMobile = false;
  programmesDropdownMobile = false;

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit() {
    this.userSubject = this.userService.user.subscribe((user: User | null) => {
      if (user) {
        this.userService.getUserById(user.id).subscribe({
          next: (currentUser) => {
            this.user = currentUser;
            this.isLoggedIn = true;
          },
          error: (error: any) => {
            console.error('Error fetching user:', error);
          },
        });
      } else {
        this.isLoggedIn = false;
        this.user = undefined;
      }
    });
  }

  toggleDropdownMenu() {
    this.showDropdownMenu = !this.showDropdownMenu;
  }

  toggleDropdown(
    dropdownName:
      | 'aboutDropdown'
      | 'programmesDropdown'
      | 'aboutDropdownMobile'
      | 'programmesDropdownMobile'
  ) {
    this[dropdownName] = !this[dropdownName];
  }

  handleLogout() {
    this.userService.logout();
    this.toggleDropdownMenu();
  }

  ngOnDestroy() {
    this.userSubject.unsubscribe();
  }
}
