import { RouterModule, Routes } from '@angular/router';

import { IsLoggedGuard } from '../RouteGuards/isLoggedGuard';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [IsLoggedGuard] },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [IsLoggedGuard],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule {}
