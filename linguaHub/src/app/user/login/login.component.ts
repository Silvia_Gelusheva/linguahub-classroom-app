import { Component } from '@angular/core';
import { LoaderService } from 'src/app/shared/loader/loader.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastService } from '../../shared/toast/toast.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  successMessage: string = 'Successfully logged in!';
  errorMessage: string = '';
  isFormVisible: boolean = true;

  constructor(
    private userService: UserService,
    private loaderService: LoaderService,
    private router: Router,
    private toast: ToastService
  ) {}

  onLoginSubmit(form: NgForm) {
    if (form.invalid) return;

    this.loaderService.show();
    this.isFormVisible = false;

    const email = form.value.email;
    const password = form.value.password;
    if (!email || !password) {
      this.errorMessage = 'Please, fill all empty form fields';
      this.loaderService.hide();
      this.isFormVisible = true;
      this.toast.error(this.errorMessage, 'Error');
      return;
    }
    this.userService.login(email, password).subscribe({
      next: () => {
        this.loaderService.hide();
        this.isFormVisible = true;
        this.toast.success(this.successMessage, 'Success');
        this.router.navigate(['']);
      },
      error: (errMsg) => {
        this.errorMessage = errMsg;
        this.loaderService.hide();
        this.isFormVisible = true;
        this.toast.error(this.errorMessage, 'Error');
      },
    });

    form.reset();
  }

  autoLoginUser() {
    const userCredentials = {
      email: 'student@test.com', // Replace with actual user email
      password: '123456', // Replace with actual user password
    };
    this.loginWithCredentials(userCredentials);
  }

  autoLoginTeacher() {
    const userCredentials = {
      email: 'teacher@test.com', // Replace with actual user email
      password: '123456', // Replace with actual user password
    };
    this.loginWithCredentials(userCredentials);
  }

  autoLoginAdmin() {
    const adminCredentials = {
      email: 'admin@test.com', // Replace with actual admin email
      password: '123456', // Replace with actual admin password
    };
    this.loginWithCredentials(adminCredentials);
  }

  private loginWithCredentials(credentials: {
    email: string;
    password: string;
  }) {
    this.loaderService.show();
    this.isFormVisible = false;

    this.userService.login(credentials.email, credentials.password).subscribe({
      next: () => {
        this.loaderService.hide();
        this.isFormVisible = true;
        this.toast.success(this.successMessage, 'Success');
        this.router.navigate(['']);
      },
      error: (errMsg) => {
        this.errorMessage = errMsg;
        this.loaderService.hide();
        this.isFormVisible = true;
        this.toast.error(this.errorMessage, 'Error');
      },
    });
  }
}
