import { Component } from '@angular/core';
import { LoaderService } from 'src/app/shared/loader/loader.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/shared/toast/toast.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent {
  successMessage = 'Registration successful';
  errorMessage: string = '';
  isFormVisible: boolean = true;

  constructor(
    private userService: UserService,
    private router: Router,
    private toast: ToastService,
    private loaderService: LoaderService
  ) {}

  onRegisterSubmit(form: NgForm) {
    if (form.invalid) return;
    this.loaderService.show();
    this.isFormVisible = false;

    const email = form.value.email;
    const password = form.value.password;
    const firstName = form.value.firstName;
    const lastName = form.value.lastName;
    const role = 4;
    if (!email || !password || !firstName || !lastName) {
      this.errorMessage = 'Please, fill all empty form fields';
    }
    this.userService
      .registerUser(email, password, firstName, lastName, role)
      .subscribe({
        next: () => {
          this.loaderService.hide();
          this.isFormVisible = true;
          this.toast.success(this.successMessage, 'Success');
          this.router.navigate(['']);
        },
        error: (err) => {
          this.loaderService.hide();
          this.isFormVisible = true;
          this.toast.error(this.errorMessage, 'Error');
        },
      });
  }
}
