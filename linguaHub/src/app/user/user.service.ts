import {
  AUTH_LOGIN_URL,
  AUTH_REGISTER_URL,
  REALTIME_URL,
} from 'src/environments/environment.development';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import { AuthResponse } from '../types/AuthResponse';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../types/User';
import { UserData } from '../types/UserData';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  user = new BehaviorSubject<User | null>(null);
  private tokenExpireTimer: any;

  constructor(private http: HttpClient, private router: Router) {}

  registerUser(
    email: string,
    password: string,
    firstName: string,
    lastName: string,
    user: number
  ): Observable<AuthResponse> {
    const requestData = { email, password, returnSecureToken: true };
    return this.http.post<AuthResponse>(AUTH_REGISTER_URL, requestData).pipe(
      tap((response) => {
        this.handleCreateUser(response);
      }),
      switchMap((response) => {
        const userData = {
          id: response.localId,
          email: email,
          firstName: firstName,
          lastName: lastName,
          role: 4,
          // user: user,
        };
        return this.http.put<AuthResponse>(
          `${REALTIME_URL}/users/${userData.id}.json`,
          userData
        );
      }),
      catchError((error) => this.handleError(error))
    );
  }

  login(email: string, password: string): Observable<AuthResponse> {
    const requestData = { email, password, returnSecureToken: true };
    return this.http.post<AuthResponse>(AUTH_LOGIN_URL, requestData).pipe(
      tap((response) => {
        this.handleCreateUser(response);
      }),
      catchError((error) => this.handleError(error))
    );
  }
  autoLogin() {
    const user = JSON.parse(localStorage.getItem('currentUser')!);
    if (!user) {
      return;
    }
    user._expiresIn = new Date(user._expiresIn);
    const loggedUser = new User(
      user.email,
      user.id,
      user._token,
      user._expiresIn
    );
    if (loggedUser.token) {
      this.user.next(loggedUser);

      const timerValue = user._expiresIn.getTime() - new Date().getTime();
      this.autoLogout(timerValue);
    }
  }

  logout() {
    this.user.next(null);
    this.router.navigate(['/auth/login']);
    localStorage.removeItem('currentUser');

    if (this.tokenExpireTimer) {
      clearTimeout(this.tokenExpireTimer);
    }
    this.tokenExpireTimer = null;
  }
  autoLogout(expireTime: number) {
    this.tokenExpireTimer = setTimeout(() => {
      this.logout();
    }, expireTime);
  }

  private handleCreateUser(response: AuthResponse) {
    const expInTs = new Date().getTime() + +response.expiresIn * 1000;
    const expiresIn = new Date(expInTs);
    const currentUser = new User(
      response.email,
      response.localId,
      response.idToken,
      expiresIn
    );
    this.user.next(currentUser);
    this.autoLogout(+response.expiresIn * 1000);

    localStorage.setItem('currentUser', JSON.stringify(currentUser));
    console.log(response, 'currentUser');
  }

  private handleError(err: any) {
    let errorMessage = 'An unknown error has occurred';
    console.log(err);
    if (!err.error || !err.error.error) {
      return throwError(() => errorMessage);
    }
    switch (err.error.error.message) {
      case 'INVALID_EMAIL':
        errorMessage = 'This email is not valid.';
        break;
      case 'EMAIL_EXISTS':
        errorMessage = 'This email already exists.';
        break;
      case 'OPERATION_NOT_ALLOWED':
        errorMessage = 'This operation is not allowed.';
        break;
      case 'INVALID_LOGIN_CREDENTIALS':
        errorMessage = 'The email ID or Password is not correct.';
        break;
    }
    return throwError(() => errorMessage);
  }

  getUserById(id: string | undefined): Observable<UserData> {
    return this.http.get<UserData>(`${REALTIME_URL}/users/${id}.json`);
  }

  isAdmin(id: string | undefined): Observable<boolean> {
    return this.getUserById(id).pipe(map((user) => user.role === 1));
  }

  isTeacher(id: string | undefined): Observable<boolean> {
    return this.getUserById(id).pipe(map((user) => user.role === 2));
  }

  isStudent(id: string | undefined): Observable<boolean> {
    return this.getUserById(id).pipe(map((user) => user.role === 3));
  }

  isTeacherOrStudent(id: string | undefined): Observable<boolean> {
    return this.getUserById(id).pipe(
      map((user) => user.role === 3 || user.role === 2)
    );
  }
}
