import { map, mergeMap, switchMap } from 'rxjs/operators';

import { CourseData } from '../types/CourseData';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { REALTIME_URL } from 'src/environments/environment.development';
import { UserData } from '../types/UserData';

@Injectable({
  providedIn: 'root',
})
export class DashboardAdminService {
  constructor(private http: HttpClient) {}

  //! MANAGE USERS IN REALTIME DATABASE

  getUsers(query: string = ''): Observable<UserData[]> {
    const url = query
      ? `${REALTIME_URL}?search=${query}`
      : `${REALTIME_URL}/users.json`;

    return this.http.get<UserData[]>(url).pipe(
      map((response: UserData[]) => {
        return Object.values(response);
      })
    );
  }

  getUserById(id: string | undefined): Observable<number | undefined> {
    return this.http
      .get<UserData | undefined>(`${REALTIME_URL}/users/${id}.json`)
      .pipe(map((userData: UserData | undefined) => userData?.role));
  }

  getUsersByIds(userIds: string[]): Observable<UserData[]> {
    return this.http.get<UserData[]>(`${REALTIME_URL}/users.json`).pipe(
      map((users) => {
        return Object.values(users).filter((user) => userIds.includes(user.id));
      })
    );
  }

  getUserByRole(role: number): Observable<UserData[]> {
    return this.http.get<UserData[]>(`${REALTIME_URL}/users.json`).pipe(
      map((users) => {
        return Object.values(users).filter((user) => user.role === role);
      })
    );
  }

  updateUser(userId: string, user: UserData): Observable<any> {
    return this.http.put(`${REALTIME_URL}/users/${userId}.json`, user);
  }

  deleteUser(userId: string) {
    return this.http.delete<UserData>(`${REALTIME_URL}/users/${userId}.json`);
  }
  isAdmin(id: string | undefined): Observable<boolean> {
    return this.getUserById(id).pipe(map((role) => role === 1));
  }

  //! MANAGE COURSES IN REALTIME DATABASE

  createCourse(course: CourseData): Observable<any> {
    return this.http.post(`${REALTIME_URL}/courses.json`, {}).pipe(
      switchMap((response: any) => {
        const courseId = response?.name;
        course.id = courseId;
        return this.http.put(
          `${REALTIME_URL}/courses/${courseId}.json`,
          course
        );
      })
    );
  }

  getCourses(query: string): Observable<CourseData[]> {
    const url = query
      ? `${REALTIME_URL}?search=${query}`
      : `${REALTIME_URL}/courses.json`;
    return this.http.get<CourseData[]>(url).pipe(
      map((response: CourseData[] | null) => {
        if (response) {
          return Object.values(response);
        } else {
          return [];
        }
      })
    );
  }

  getCourseById(courseId: string): Observable<CourseData> {
    return this.http.get<CourseData>(
      `${REALTIME_URL}/courses/${courseId}.json`
    );
  }

  addStudentToCourse(courseId: string, studentId: string): Observable<any> {
    return this.http
      .get<string[]>(`${REALTIME_URL}/courses/${courseId}/students.json`)
      .pipe(
        mergeMap((students: string[]) => {
          if (!students) {
            students = [];
          }
          students.push(studentId);
          return this.http.put(
            `${REALTIME_URL}/courses/${courseId}/students.json`,
            students
          );
        })
      );
  }

  addCourseToStudent(courseId: string, studentId: string): Observable<any> {
    return this.http.patch(`${REALTIME_URL}/users/${studentId}.json`, {
      assignedTo: courseId,
    });
  }

  removeStudentFromCourse(studentId: string) {
    return this.http.delete(
      `${REALTIME_URL}/users/${studentId}/assignedTo.json`
    );
  }

  updateCourse(
    courseId: string,
    courseData: CourseData
  ): Observable<CourseData> {
    return this.http.put<CourseData>(
      `${REALTIME_URL}/courses/${courseId}.json`,
      courseData
    );
  }

  deleteCourse(courseId: string) {
    return this.http.delete(`${REALTIME_URL}/courses/${courseId}.json`);
  }
}
