import { RouterModule, Routes } from '@angular/router';

import { AdminGuard } from '../RouteGuards/adminGuard';
import { AssignStudentComponent } from './assign-student/assign-student.component';
import { AuthGuard } from '../RouteGuards/authGuard';
import { CourseComponent } from './course/course.component';
import { CoursesComponent } from './courses/courses.component';
import { CreateCourseComponent } from './create-course/create-course.component';
import { DashboardAdminComponent } from './dashboard-admin/dashboard-admin.component';
import { NgModule } from '@angular/core';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardAdminComponent,
    canActivate: [AuthGuard, AdminGuard],
    children: [
      { path: 'users', component: UsersComponent },
      { path: 'courses', component: CoursesComponent },
      { path: 'courses/:id', component: CourseComponent },
      { path: 'create-course', component: CreateCourseComponent },
      { path: 'assign-student', component: AssignStudentComponent },
      { path: '', redirectTo: 'users', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardAdminRoutingModule {}
