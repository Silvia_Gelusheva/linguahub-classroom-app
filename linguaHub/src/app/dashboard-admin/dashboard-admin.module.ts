import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AssignStudentComponent } from './assign-student/assign-student.component';
import { CardDetailsComponent } from './course/card-details/card-details.component';
import { CommonModule } from '@angular/common';
import { CourseComponent } from './course/course.component';
import { CoursesComponent } from './courses/courses.component';
import { CreateCourseComponent } from './create-course/create-course.component';
import { DashboardAdminComponent } from './dashboard-admin/dashboard-admin.component';
import { DashboardAdminRoutingModule } from './dashboard-admin-routing.module';
import { EditModeComponent } from './course/edit-mode/edit-mode.component';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSidenavModule } from '@angular/material/sidenav';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { UsersComponent } from './users/users.component';

@NgModule({
  declarations: [
    DashboardAdminComponent,
    UsersComponent,
    CoursesComponent,
    CreateCourseComponent,
    AssignStudentComponent,
    CourseComponent,
    CardDetailsComponent,
    EditModeComponent,
  ],
  imports: [
    CommonModule,
    DashboardAdminRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatCardModule,
    MatIconModule,
    MatSidenavModule,
  ],
})
export class DashboardAdminModule {}
