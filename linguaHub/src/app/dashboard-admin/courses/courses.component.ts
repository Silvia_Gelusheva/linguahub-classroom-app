import { Component, OnInit } from '@angular/core';

import { CourseData } from 'src/app/types/CourseData';
import { DashboardAdminService } from '../dashboard-admin.service';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css'],
})
export class CoursesComponent implements OnInit {
  courses: CourseData[] = [];
  filteredCourses: CourseData[] = [];
  currentPage = 0;
  pageSize = 5;
  totalCourses = 0;

  constructor(private adminService: DashboardAdminService) {}

  ngOnInit(): void {
    this.fetchCourses('');
  }

  fetchCourses(query: string) {
    this.adminService.getCourses(query).subscribe((response) => {
      this.courses = response;
      this.filteredCourses = [...this.courses];
      this.updateFilteredCourses();
    });
  }

  handlePageEvent(pageEvent: PageEvent) {
    this.currentPage = pageEvent.pageIndex;
    this.pageSize = pageEvent.pageSize;
    this.updateFilteredCourses();
  }

  updateFilteredCourses() {
    const startIndex = this.currentPage * this.pageSize;
    const endIndex = startIndex + this.pageSize;
    this.filteredCourses = [...this.courses.slice(startIndex, endIndex)];
  }

  filterCourses(query: string) {
    if (query.trim() === '') {
      this.filteredCourses = [...this.courses];
      this.totalCourses = this.filteredCourses.length;
      this.currentPage = 0;
    } else {
      this.filteredCourses = this.courses.filter(
        (course) =>
          course.name.toLowerCase().includes(query.toLowerCase()) ||
          course.teacher.toLowerCase().includes(query.toLowerCase())
      );
      this.totalCourses = this.filteredCourses.length;
      this.currentPage = 0;
    }
  }
}
