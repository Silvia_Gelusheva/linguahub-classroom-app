import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription, forkJoin } from 'rxjs';

import { CourseData } from '../../types/CourseData';
import { DashboardAdminService } from '../dashboard-admin.service';
import { NgForm } from '@angular/forms';
import { ToastService } from 'src/app/shared/toast/toast.service';
import { UserData } from '../../types/UserData';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-assign-student',
  templateUrl: './assign-student.component.html',
  styleUrls: ['./assign-student.component.css'],
})
export class AssignStudentComponent implements OnInit, OnDestroy {
  courses: CourseData[] = [];
  students: UserData[] = [];
  successMessage: string = '';
  errorMessage: string = '';

  subscription: Subscription | undefined;
  constructor(
    private adminService: DashboardAdminService,
    private toast: ToastService
  ) {}

  ngOnInit(): void {
    this.loadInitialData();
  }

  loadInitialData(): void {
    this.adminService.getUserByRole(3).subscribe({
      next: (response) => {
        this.students = Object.values(response).filter(
          (student) => !student.assignedTo
        );
      },
      error: (err) => {
        this.errorMessage = err;
        this.toast.error(this.errorMessage, 'Error');
      },
    });

    this.adminService.getCourses('').subscribe({
      next: (response) => {
        this.courses = Object.values(response);
      },
      error: (err) => {
        this.errorMessage = err;
        this.toast.error(this.errorMessage, 'Error');
      },
    });
  }

  addStudent(formData: NgForm) {
    if (!formData.value || !formData.valid) {
      this.errorMessage = 'Invalid form! Please fill all inputs correctly';
      return this.toast.error(this.errorMessage, 'Error');
    } else {
      this.successMessage = 'Student successfully assigned to course!';
      this.toast.success(this.successMessage, 'Success');
    }

    const courseId: string = formData.value.course;
    const studentId: string = formData.value.student;

    forkJoin([
      this.adminService.addCourseToStudent(courseId, studentId),
      this.adminService.addStudentToCourse(courseId, studentId),
    ])
      .pipe(
        switchMap(() => {
          return forkJoin([
            this.adminService.getUserByRole(3),
            this.adminService.getCourses(''),
          ]);
        })
      )
      .subscribe({
        next: ([usersResponse, coursesResponse]) => {
          formData.resetForm();
          this.students = Object.values(usersResponse).filter(
            (student) => !student.assignedTo
          );
          this.courses = Object.values(coursesResponse);
          // this.router.navigate(['/dashboard/courses']);
        },
        error: (err) => {
          this.errorMessage = err;
          this.toast.error(this.errorMessage, 'Error');
        },
      });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}

/* from rxjs docs: ForkJoin - accepts an Array of ObservableInput or a dictionary Object of ObservableInput and returns an
     Observable that emits either an array of values in the exact same order as the passed array,
      or a dictionary of values in the same shape as the passed dictionary.*/

/*from rxjs docs: SwitchMap - projects each source value to an Observable which is merged in the output Observable, emitting values
         only from the most recently projected Observable.*/
