import { Component, OnInit } from '@angular/core';

import { CourseData } from 'src/app/types/CourseData';
import { DashboardAdminService } from '../dashboard-admin.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ToastService } from 'src/app/shared/toast/toast.service';
import { UserData } from 'src/app/types/UserData';

@Component({
  selector: 'app-create-course',
  templateUrl: './create-course.component.html',
  styleUrls: ['./create-course.component.css'],
})
export class CreateCourseComponent implements OnInit {
  teachers: UserData[] = [];
  courses: CourseData[] = [];
  successMessage: string = '';
  errorMessage: string = '';

  private subscription: Subscription | undefined;

  constructor(
    private adminService: DashboardAdminService,
    private toast: ToastService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.adminService.getUserByRole(2).subscribe({
      next: (users: UserData[]) => {
        this.teachers = users;
      },
      error: (error: any) => {
        console.error(error.message);
      },
    });

    this.adminService.getCourses('').subscribe({
      next: (res) => {
        this.courses = Object.values(res);
      },
    });
  }

  validateDates(startDate: string, endDate: string): string | boolean {
    const start = new Date(startDate);
    const end = new Date(endDate);
    const today = new Date();

    if (start >= end) {
      return 'Start date must be before the end date.';
    }

    if (start < today) {
      return 'Start date must be after today.';
    }

    if (today > end) {
      return 'End date cannot be before today.';
    }

    const diffInMonths =
      (end.getFullYear() - start.getFullYear()) * 12 +
      (end.getMonth() - start.getMonth());
    if (diffInMonths < 1) {
      return 'The duration between start and end date must be at least 1 month.';
    }

    return true;
  }
  validateCourseName(name: string): boolean {
    if (this.courses.some((c) => c.name === name)) return true;
    return false;
  }

  addNewClass(formData: NgForm): void {
    const courseData: CourseData = {
      ...formData.value,
    };

    if (!formData.value || !formData.valid) {
      this.errorMessage = 'Invalid form! Please fill all inputs correctly';
      return this.toast.error(this.errorMessage, 'Error');
    } else {
      this.successMessage = 'Course successfully created!';
      this.toast.success(this.successMessage, 'Success');
    }

    this.adminService.createCourse(courseData).subscribe({
      next: () => {
        formData.resetForm();
        this.router.navigate(['/dashboard/courses']);
      },
      error: (error) => {
        this.toast.error(error, 'Error');
      },
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
