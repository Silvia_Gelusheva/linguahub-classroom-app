import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { CourseData } from 'src/app/types/CourseData';
import { UserData } from 'src/app/types/UserData';

@Component({
  selector: 'app-edit-mode',
  templateUrl: './edit-mode.component.html',
  styleUrls: ['./edit-mode.component.css'],
})
export class EditModeComponent implements OnInit {
  @Input() course: CourseData | undefined;
  @Input() teachers: UserData[] = [];
  @Input() students: UserData[] = [];
  @Input() selectedIndexes: number[] = [];
  @Input() courseForm: FormGroup;
  @Output() updateCourse = new EventEmitter<FormGroup>();
  @Output() deleteStudent = new EventEmitter<{
    studentId: string;
    index: number;
    event: MouseEvent;
  }>();

  constructor(private fb: FormBuilder) {
    this.courseForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(7)]],
      teacher: ['', [Validators.required]],
      type: ['', [Validators.required]],
      level: ['', [Validators.required]],
      startDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]],
      students: [[]],
    });
  }

  ngOnInit(): void {
    if (this.course) {
      this.courseForm.patchValue({
        name: this.course.name,
        teacher: this.course.teacher,
        type: this.course.type,
        level: this.course.level,
        startDate: this.course.startDate,
        endDate: this.course.endDate,
        students: this.course.students,
      });
    }
  }

  onSubmit() {
    if (this.courseForm.valid) {
      this.updateCourse.emit(this.courseForm);
    }
  }

  deleteStudentHandler(studentId: string, index: number, event: MouseEvent) {
    this.deleteStudent.emit({ studentId, index, event });
  }
}
