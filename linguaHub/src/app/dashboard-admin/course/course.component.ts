import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';
import { CourseData } from 'src/app/types/CourseData';
import { DashboardAdminService } from '../dashboard-admin.service';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from 'src/app/shared/toast/toast.service';
import { UserData } from 'src/app/types/UserData';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css'],
})
export class CourseComponent implements OnInit {
  courseId = this.route.snapshot.paramMap.get('id');
  course: CourseData | undefined;
  studentsIds: string[] | undefined;
  students: UserData[] = [];
  teachers: UserData[] = [];
  markedIdsForDeletion: string[] = [];
  selectedIndexes: number[] = [];
  courseForm: FormGroup;
  editMode = false;
  isMarked = false;
  successMessage = '';
  errorMessage = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toast: ToastService,
    private adminService: DashboardAdminService,
    private dialog: MatDialog,
    private fb: FormBuilder
  ) {
    this.courseForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(7)]],
      teacher: ['', [Validators.required]],
      type: ['', [Validators.required]],
      level: ['', [Validators.required]],
      startDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]],
      students: [[]],
    });
  }

  ngOnInit(): void {
    if (!this.courseId) return;

    this.adminService.getCourseById(this.courseId).subscribe({
      next: (response) => {
        this.course = response;
        this.studentsIds = response.students;

        if (this.studentsIds) {
          this.adminService.getUsersByIds(this.studentsIds).subscribe({
            next: (response) => {
              this.students = response;
            },
            error: () => {
              this.toast.error('Error fetching users', 'Error');
            },
          });
        }

        this.courseForm.patchValue({
          name: this.course.name,
          teacher: this.course.teacher,
          type: this.course.type,
          level: this.course.level,
          startDate: this.course.startDate,
          endDate: this.course.endDate,
          students: this.course.students,
        });
      },
    });

    this.adminService.getUserByRole(2).subscribe({
      next: (users: UserData[]) => {
        this.teachers = users;
      },
      error: () => {
        this.toast.error('Error fetching teachers', 'Error');
      },
    });
  }

  onToggle() {
    this.editMode = !this.editMode;
  }

  deleteStudent(studentId: string, index: number, event: MouseEvent) {
    event.preventDefault();
    if (this.selectedIndexes === null) {
      this.selectedIndexes = [];
    }

    const selectedIndex = this.selectedIndexes.indexOf(index);
    if (selectedIndex !== -1) {
      this.selectedIndexes.splice(selectedIndex, 1);
    } else {
      this.selectedIndexes.push(index);
    }

    const studentIds = this.courseForm.get('students')?.value;
    const updatedStudentIds = studentIds.filter(
      (id: string) => id !== studentId
    );
    this.isMarked = !this.isMarked;
    this.markedIdsForDeletion.push(studentId);
    this.courseForm.patchValue({ students: updatedStudentIds });
  }

  handleUpdateCourse(courseForm: FormGroup) {
    if (courseForm.invalid || !this.course || !this.courseId) {
      this.errorMessage =
        'Updating course denied! Please fill all inputs correctly!';
      return this.toast.error(this.errorMessage, 'Error');
    }

    const markedStudentIds = this.markedIdsForDeletion;

    const filteredStudentIds = this.course.students?.filter(
      (id) => !markedStudentIds.includes(id)
    );

    const newCourseData: CourseData = {
      id: this.courseId,
      name: this.courseForm.value.name,
      teacher: this.courseForm.value.teacher,
      type: this.courseForm.value.type,
      level: this.courseForm.value.level,
      startDate: this.courseForm.value.startDate,
      endDate: this.courseForm.value.endDate,
      students: filteredStudentIds,
    };

    this.adminService.updateCourse(this.courseId, newCourseData).subscribe({
      next: () => {
        this.adminService.getCourseById(this.courseId!).subscribe({
          next: (response) => {
            this.course = response;
            this.studentsIds = filteredStudentIds;

            if (this.studentsIds) {
              this.adminService.getUsersByIds(this.studentsIds).subscribe({
                next: (response) => {
                  this.students = response;
                },
                error: () => {
                  this.toast.error('Error fetching users', 'Error');
                },
              });
            }
            this.selectedIndexes = [];
            this.toast.success('Course successfully updated!', 'Success');
            this.editMode = false;
          },
        });
      },
      error: () => {
        this.toast.error('Error updating course', 'Error');
      },
    });

    markedStudentIds.forEach((studentId) => {
      this.adminService.removeStudentFromCourse(studentId).subscribe({
        next: () => {
          this.markedIdsForDeletion = [];
        },
      });
    });
  }

  handleDeleteCourse() {
    if (!this.courseId) return;

    this.adminService.deleteCourse(this.courseId).subscribe({
      next: () => {
        if (
          this.students &&
          Array.isArray(this.students) &&
          this.students.length > 0
        ) {
          const deleteObservables = this.students.map((student) => {
            return this.adminService.removeStudentFromCourse(student.id);
          });

          forkJoin(deleteObservables).subscribe({
            next: () => {
              this.router.navigate(['/dashboard/courses']);
            },
            error: (error) => {
              this.toast.error(error.message, 'Error');
            },
          });
        } else {
          this.router.navigate(['/dashboard/courses']);
        }
      },
      error: (error) => {
        this.toast.error(error.message, 'Error');
      },
    });
  }

  confirmDelete(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      data: { message: 'Are you sure you want to delete this course?' },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.handleDeleteCourse();
      }
    });
  }
}
