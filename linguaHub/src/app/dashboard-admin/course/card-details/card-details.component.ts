import { Component, EventEmitter, Input, Output } from '@angular/core';

import { CourseData } from 'src/app/types/CourseData';
import { UserData } from 'src/app/types/UserData';

@Component({
  selector: 'app-card-details',
  templateUrl: './card-details.component.html',
  styleUrls: ['./card-details.component.css'],
})
export class CardDetailsComponent {
  @Input() students: UserData[] | undefined;
  @Input() course: CourseData = {
    id: '',
    name: '',
    teacher: '',
    type: '',
    level: 0,
  };
}
