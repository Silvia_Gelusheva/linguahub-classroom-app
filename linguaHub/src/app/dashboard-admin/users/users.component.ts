import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';
import { DashboardAdminService } from '../dashboard-admin.service';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { UserData } from 'src/app/types/UserData';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {
  users: UserData[] = [];
  filteredUsers: UserData[] = [];
  currentPage = 0;
  pageSize = 5;
  totalUsers = 0;
  showDropdown: boolean = false;

  isEditMode: { [key: string]: boolean } = {};
  roleForms: { [key: string]: FormGroup } = {};

  constructor(
    private adminService: DashboardAdminService,
    private fb: FormBuilder,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.getUsers('');
  }

  handlePageEvent(pageEvent: PageEvent) {
    this.currentPage = pageEvent.pageIndex;
    this.pageSize = pageEvent.pageSize;
    this.updateFilteredUsers();
  }

  getUsers(query: string) {
    this.adminService.getUsers(query).subscribe((response) => {
      this.users = response;
      this.filteredUsers = [...this.users];
      this.totalUsers = this.users.length;
      this.updateFilteredUsers();
      this.initializeForms();
    });
  }

  initializeForms() {
    this.users.forEach((user) => {
      this.roleForms[user.id] = this.fb.group({
        role: [user.role],
      });
      this.isEditMode[user.id] = false;
    });
  }

  filterUsers(query: string) {
    if (query.trim() === '') {
      this.filteredUsers = [...this.users];
      this.totalUsers = this.filteredUsers.length;
      this.currentPage = 0;
    } else {
      this.filteredUsers = this.users.filter(
        (user) =>
          user.firstName.toLowerCase().includes(query.toLowerCase()) ||
          user.lastName.toLowerCase().includes(query.toLowerCase()) ||
          user.email.toLowerCase().includes(query.toLowerCase())
      );
      this.totalUsers = this.filteredUsers.length;
      this.currentPage = 0;
      this.showDropdown = !this.showDropdown;
    }
  }

  filterUsersByRole(query: number) {
    if (query !== 0) {
      this.filteredUsers = this.users.filter((user) => user.role === query);
    } else {
      this.filteredUsers = [...this.users];
    }
    this.totalUsers = this.filteredUsers.length;
    this.currentPage = 0;
  }

  updateFilteredUsers() {
    const startIndex = this.currentPage * this.pageSize;
    const endIndex = startIndex + this.pageSize;
    this.filteredUsers = [...this.users.slice(startIndex, endIndex)];
  }

  onToggle(userId: string) {
    this.isEditMode[userId] = true;
  }

  updateRole(user: UserData, userId: string, e: MouseEvent) {
    e.preventDefault();
    const newRole = this.roleForms[userId].value.role;
    if (newRole !== null && newRole !== undefined) {
      user.role = +newRole;
      if (user.id) {
        this.adminService.updateUser(user.id, user).subscribe({
          next: () => {
            this.isEditMode[userId] = false;
            this.getUsers('');
          },
          error: (error) => {
            console.error('Error updating role:', error);
          },
        });
      } else {
        console.error('User ID is undefined.');
      }
    }
  }

  handleDeleteUser(userId: string | undefined) {
    if (!userId) return;
    this.adminService.deleteUser(userId).subscribe({
      next: () => {
        this.users = this.users.filter((user) => user.id !== userId);
        this.filterUsers('');
      },
    });
  }

  confirmDelete(userId: string | undefined): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      data: { message: 'Are you sure you want to delete this user?' },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.handleDeleteUser(userId);
      }
    });
  }
}
