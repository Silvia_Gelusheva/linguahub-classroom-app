import { Component } from '@angular/core';

@Component({
  selector: 'app-adults',
  templateUrl: './adults.component.html',
  styleUrls: ['./adults.component.css'],
})
export class AdultsComponent {
  coursesData: any[] = [
    {
      title: 'Beginners',
      url: 'assets/adult-b.jpg',
      desc: 'Embrace the journey of mastering English, the global language of communication, through our Common English course designed for learners of all levels. Explore fundamental language skills such as listening, speaking, reading, and writing in a supportive and interactive learning environment.      Engage in practical exercises and activities that reinforce vocabulary, grammar, and pronunciation, enabling you to communicate confidently in everyday situations.      Immerse yourself in authentic English conversations and cultural insights, enhancing your ability to connect with others from diverse backgrounds.      Join a vibrant community of language enthusiasts and embark on a rewarding learning experience that opens doors to new opportunities and connections worldwide.',
    },
    {
      title: 'Intermediate',
      url: 'assets/adult-i.jpg',
      desc: 'Embrace the journey of mastering English, the global language of communication, through our Common English course designed for learners of all levels. Explore fundamental language skills such as listening, speaking, reading, and writing in a supportive and interactive learning environment.      Engage in practical exercises and activities that reinforce vocabulary, grammar, and pronunciation, enabling you to communicate confidently in everyday situations.      Immerse yourself in authentic English conversations and cultural insights, enhancing your ability to connect with others from diverse backgrounds.      Join a vibrant community of language enthusiasts and embark on a rewarding learning experience that opens doors to new opportunities and connections worldwide.',
    },
    {
      title: 'Advanced',
      url: 'assets/adult-a.jpg',
      desc: 'Embrace the journey of mastering English, the global language of communication, through our Common English course designed for learners of all levels. Explore fundamental language skills such as listening, speaking, reading, and writing in a supportive and interactive learning environment.      Engage in practical exercises and activities that reinforce vocabulary, grammar, and pronunciation, enabling you to communicate confidently in everyday situations.      Immerse yourself in authentic English conversations and cultural insights, enhancing your ability to connect with others from diverse backgrounds.      Join a vibrant community of language enthusiasts and embark on a rewarding learning experience that opens doors to new opportunities and connections worldwide.',
    },
  ];
  getCardClass(index: number): string {
    return index % 2 === 0 ? 'left-image' : 'right-image';
  }
}
