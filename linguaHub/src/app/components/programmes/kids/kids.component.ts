import { Component } from '@angular/core';

@Component({
  selector: 'app-kids',
  templateUrl: './kids.component.html',
  styleUrls: ['./kids.component.css'],
})
export class KidsComponent {
  coursesData: any[] = [
    {
      title: 'Beginners',
      url: 'assets/kids-b.jpg',
      desc: 'Dive into the world of English language learning with our dynamic course designed specifically for young beginners.Engage in fun and interactive activities that stimulate curiosity and encourage active participation. Develop foundational language skills through a tailored curriculum that focuses on building vocabulary and basic grammar structures.Immerse yourself in a supportive learning environment where creativity and exploration are celebrated.Join us on a journey of discovery as we embark on exciting adventures in language and culture, igniting a lifelong passion for English learning.',
    },
    {
      title: 'Intermediate',
      url: 'assets/kids-i.jpg',
      desc: 'Dive into the world of English language learning with our dynamic course designed specifically for young beginners.Engage in fun and interactive activities that stimulate curiosity and encourage active participation. Develop foundational language skills through a tailored curriculum that focuses on building vocabulary and basic grammar structures.Immerse yourself in a supportive learning environment where creativity and exploration are celebrated.Join us on a journey of discovery as we embark on exciting adventures in language and culture, igniting a lifelong passion for English learning.',
    },
    {
      title: 'Advanced',
      url: 'assets/kids-a.jpg',
      desc: 'Dive into the world of English language learning with our dynamic course designed specifically for young beginners.Engage in fun and interactive activities that stimulate curiosity and encourage active participation. Develop foundational language skills through a tailored curriculum that focuses on building vocabulary and basic grammar structures.Immerse yourself in a supportive learning environment where creativity and exploration are celebrated.Join us on a journey of discovery as we embark on exciting adventures in language and culture, igniting a lifelong passion for English learning.',
    },
  ];

  getCardClass(index: number): string {
    return index % 2 === 0 ? 'left-image' : 'right-image';
  }
}
