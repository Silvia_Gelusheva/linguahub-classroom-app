import { AfterViewInit, Component } from '@angular/core';

@Component({
  selector: 'app-corporate',
  templateUrl: './corporate.component.html',
  styleUrls: ['./corporate.component.css'],
})
export class CorporateComponent implements AfterViewInit {
  coursesData: any[] = [
    {
      title: 'General English',
      url: 'assets/general-eng.jpg',
      desc: 'Embrace the journey of mastering English, the global language of communication, through our Common English course designed for learners of all levels. Explore fundamental language skills such as listening, speaking, reading, and writing in a supportive and interactive learning environment.      Engage in practical exercises and activities that reinforce vocabulary, grammar, and pronunciation, enabling you to communicate confidently in everyday situations.      Immerse yourself in authentic English conversations and cultural insights, enhancing your ability to connect with others from diverse backgrounds.      Join a vibrant community of language enthusiasts and embark on a rewarding learning experience that opens doors to new opportunities and connections worldwide.',
    },
    {
      title: 'Business English',
      url: 'assets/work-eng.jpg',
      desc: "Elevate your professional communication skills with our comprehensive Business English course, tailored for professionals seeking proficiency in workplace interactions.      Acquire essential language tools and strategies to effectively navigate corporate environments, from meetings and presentations to negotiations and networking.      Immerse yourself in real-world scenarios and case studies, honing your ability to articulate ideas, collaborate with colleagues, and engage with clients on a global scale.      Gain confidence in expressing yourself fluently and persuasively in English, empowering you to thrive in diverse business contexts and advance your career ambitions. Unlock the doors to new opportunities and enhance your competitive edge in today's interconnected global marketplace with our Business English course.",
    },
  ];
  getCardClass(index: number): string {
    return index % 2 === 0 ? 'left-image' : 'right-image';
  }

  ngAfterViewInit() {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          entry.target.classList.add('visible');
        } else {
          entry.target.classList.remove('visible');
        }
      });
    });

    document.querySelectorAll('.animate-on-scroll').forEach((element) => {
      observer.observe(element);
    });
  }
}
