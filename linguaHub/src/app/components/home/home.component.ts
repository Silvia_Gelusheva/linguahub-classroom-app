import { AfterViewInit, Component } from '@angular/core';

import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/types/User';
import { UserService } from 'src/app/user/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements AfterViewInit {
  constructor(private userService: UserService, private router: Router) {}
  isLoggedIn: boolean = false;
  private userSubject!: Subscription;

  ngOnInit() {
    this.userSubject = this.userService.user.subscribe((user: User | null) => {
      this.isLoggedIn = user ? true : false;
    });
  }

  ngAfterViewInit() {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          entry.target.classList.add('visible');
        } else {
          entry.target.classList.remove('visible');
        }
      });
    });

    document.querySelectorAll('.animate-on-scroll').forEach((element) => {
      observer.observe(element);
    });
  }

  handleSignIn() {
    this.router.navigate(['/auth/login']);
  }

  handleLogout() {
    this.userService.logout();
  }

  ngOnDestroy() {
    this.userSubject.unsubscribe();
  }
}
