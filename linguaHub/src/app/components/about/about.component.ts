import { Component } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css'],
})
export class AboutComponent {
  data: { id: number; title: string; text: string }[] = [
    {
      id: 1,
      title: 'Mission',
      text: 'Our mission is to empower individuals with the language skills they need to thrive in an interconnected world. Through innovative teaching methods, personalized instruction, and a supportive learning environment, we strive to cultivate confidence and proficiency in English language communication. Our goal is not only to impart linguistic knowledge but also to foster cultural understanding and global citizenship. We are committed to equipping our students with the tools they need to succeed academically, professionally, and personally, as they navigate a diverse and dynamic global landscape.',
    },
    {
      id: 2,
      title: 'Vision',
      text: 'In envisioning the future of our English Language Academy, we see a vibrant community of lifelong learners who are fluent, confident, and culturally aware communicators. Our vision is to be a leading institution renowned for excellence in English language education, recognized globally for our innovative approaches and impactful contributions to language learning. We aspire to create a nurturing environment where creativity, critical thinking, and collaboration thrive, empowering our students to unlock new opportunities and realize their fullest potential. Through continuous innovation, research, and partnerships, we aim to stay at the forefront of language education, shaping the next generation of global leaders who can effectively engage and contribute to an increasingly interconnected world.',
    },
    {
      id: 3,
      title: 'Goals',
      text: 'At our English Language Academy, our goals are multifaceted and dynamic, designed to continually elevate the quality of education and support provided to our students. Firstly, we aim to enhance student engagement and satisfaction by offering dynamic and interactive learning experiences that cater to diverse learning styles and preferences. Secondly, we strive to maintain academic excellence by employing highly qualified instructors, implementing cutting-edge teaching methodologies, and regularly updating our curriculum to reflect the latest developments in language education. Additionally, we are committed to fostering a supportive and inclusive learning environment that promotes cultural understanding, empathy, and respect among students from various backgrounds. Furthermore, we seek to expand our reach and impact by leveraging technology and strategic partnerships to offer accessible and flexible learning opportunities to individuals worldwide. Ultimately, our overarching goal is to empower our students with the language skills, cultural competence, and confidence they need to succeed in their academic, professional, and personal pursuits, both now and in the future.',
    },
    {
      id: 4,
      title: 'Environment',
      text: 'The environment at our English Language Academy is designed to foster a sense of community, collaboration, and growth. Our classrooms are dynamic spaces where students from diverse backgrounds come together to learn and engage in meaningful language acquisition activities. Through interactive lessons, group discussions, and hands-on learning experiences, we create an atmosphere that encourages active participation and peer-to-peer interaction. Our instructors are not only highly qualified educators but also supportive mentors who are dedicated to the success and well-being of each student. Beyond the classroom, our academy provides various opportunities for extracurricular activities, cultural events, and language immersion experiences, allowing students to further enhance their language skills while developing meaningful connections with their peers. Whether on-campus or online, we prioritize creating a safe, inclusive, and welcoming environment where every student feels valued, supported, and empowered to achieve their language learning goals.',
    },
  ];
}
