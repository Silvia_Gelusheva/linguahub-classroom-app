import { AfterViewInit, Component } from '@angular/core';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.css'],
})
export class GeneralComponent implements AfterViewInit {
  data: { id: number; image: string; title: string; text: string }[] = [
    {
      id: 1,
      image:
        'https://icons.veryicon.com/png/o/object/color-game-icon/space-mission.png',
      title: 'Mission',
      text: 'Our mission is to empower individuals with the language skills they need to thrive in an interconnected world. Through innovative teaching methods, personalized instruction, and a supportive learning environment, we strive to cultivate confidence and proficiency in English language communication. Our goal is not only to impart linguistic knowledge but also to foster cultural understanding and global citizenship. We are committed to equipping our students with the tools they need to succeed academically, professionally, and personally, as they navigate a diverse and dynamic global landscape.',
    },
    {
      id: 2,
      image: 'https://cdn-icons-png.freepik.com/512/199/199063.png',
      title: 'Vision',
      text: 'In envisioning the future of our Academy, we see a vibrant community of lifelong learners who are fluent, confident, and culturally aware communicators. Our vision is to be a leading institution renowned for excellence in English language education, recognized globally for our innovative approaches and impactful contributions to language learning. We aspire to create a nurturing environment where creativity, critical thinking, and collaboration thrive, empowering our students to unlock new opportunities and realize their fullest potential.',
    },
    {
      id: 3,
      image:
        'https://static-00.iconduck.com/assets.00/goal-icon-2045x2048-hp8ur243.png',
      title: 'Goals',
      text: 'At our English Language Academy, our goals are multifaceted and dynamic, designed to continually elevate the quality of education and support provided to our students. Firstly, we aim to enhance student engagement and satisfaction by offering dynamic and interactive learning experiences that cater to diverse learning styles and preferences. Secondly, we strive to maintain academic excellence by employing highly qualified instructors, implementing cutting-edge teaching methodologies, and regularly updating our curriculum to reflect the latest developments in language education. ',
    },
    {
      id: 4,
      image:
        'https://i.pinimg.com/originals/71/c7/53/71c7533cc831bf2d6ddffdd74cd117bb.png',
      title: 'Environment',
      text: 'In envisioning the future of our English Language Academy, we see a vibrant community of lifelong learners who are fluent, confident, and culturally aware communicators. Our vision is to be a leading institution renowned for excellence in English language education, recognized globally for our innovative approaches and impactful contributions to language learning. We aspire to create a nurturing environment where creativity, critical thinking, and collaboration thrive, empowering our students to unlock new opportunities and realize their fullest potential.  ',
    },
  ];

  ngAfterViewInit() {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          entry.target.classList.add('visible');
        } else {
          entry.target.classList.remove('visible');
        }
      });
    });

    document.querySelectorAll('.animate-on-scroll').forEach((element) => {
      observer.observe(element);
    });
  }
}
