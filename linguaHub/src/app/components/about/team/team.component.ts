import { AfterViewInit, Component } from '@angular/core';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css'],
})
export class TeamComponent implements AfterViewInit {
  data: {
    id: number;
    image: string;
    name: string;
    position: string;
    bio: string;
    contacts: string;
    phone: string;
  }[] = [
    {
      id: 1,
      image:
        'https://images.immediate.co.uk/production/volatile/sites/3/2021/09/daniel-craig-007.jpg-303a730.png',
      name: 'Bond. James Bond',
      position: 'Chief Executive Manager',
      bio: 'As the head of our organization, the general manager is responsible for setting goals, developing strategies, and overseeing all aspects of our operations. Their vision and expertise drive our team towards excellence.',
      contacts: `bond@test.com`,
      phone: '000-000-777',
    },
    {
      id: 2,
      image:
        'https://static.independent.co.uk/s3fs-public/thumbnails/image/2017/05/22/08/johnny-english.jpg',
      name: 'Mr. Johnny English',
      position: 'Office manager',
      bio: 'Diligent and strategic, our manager oversees the daily operations, ensuring efficiency and productivity. With a keen eye for detail and strong leadership skills, they guide our team towards success.',
      contacts: 'english@test.com',
      phone: '000-000-778',
    },
    {
      id: 3,
      image:
        'https://i.guim.co.uk/img/media/ffc016b01f45eeec94ff69dc59eb65a9137ae52a/0_95_3500_2101/master/3500.jpg?width=1200&height=900&quality=85&auto=format&fit=crop&s=915d4080291d752325c0a25518ac4602',
      name: 'Mr. Sherlock Holmes',
      position: 'Research manager',
      bio: 'Our research manager is the backbone of our organization, handling various administrative tasks with precision and organization. Their multitasking abilities keep our office running smoothly.',
      contacts: 'holmes@test.com',
      phone: '012-345-789',
    },
    {
      id: 4,
      image:
        'https://cdn.kayiprihtim.com/wp-content/uploads/2022/10/doktor-watson-dizisi-sherlock.jpg',
      name: 'Dr. John H. Watson',
      position: 'Assistant almighty',
      bio: 'Supportive and resourceful, our office assistant provides valuable assistance to our team, handling clerical tasks, scheduling appointments, and ensuring the office environment remains organized and efficient.',
      contacts: 'watson@test.com',
      phone: '100-000-000',
    },
  ];

  ngAfterViewInit() {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          entry.target.classList.add('visible');
        } else {
          entry.target.classList.remove('visible');
        }
      });
    });

    document.querySelectorAll('.animate-on-scroll').forEach((element) => {
      observer.observe(element);
    });
  }
}
