import { AfterViewInit, Component } from '@angular/core';

@Component({
  selector: 'app-careers',
  templateUrl: './careers.component.html',
  styleUrls: ['./careers.component.css'],
})
export class CareersComponent implements AfterViewInit {
  data: {
    id: number;
    image: string;
    title: string;
    experience: string;
    education: string;
    text: string;
  }[] = [
    {
      id: 1,
      image:
        'https://img.freepik.com/free-photo/corporate-management-strategy-solution-branding-concept_53876-167088.jpg?size=626&ext=jpg&ga=GA1.1.2116175301.1717977600&semt=sph',
      title: 'Marketing Assistant',
      experience: '0-2 years',
      education:
        'Education in the field of economics, the humanities, or other areas',
      text: "Are you creative, organized, and passionate about promoting educational programs? Look no further! The English Language Academy is hiring a Marketing Assistant to join our dynamic team. In this role, you'll collaborate with our marketing team to develop and implement promotional campaigns that showcase the value of our language courses and programs. From social media management to event coordination, you'll play a crucial role in spreading the word about our academy's offerings. If you're ready to dive into the world of educational marketing and make a meaningful impact, we invite you to apply today!",
    },
    {
      id: 2,
      image:
        'https://st2.depositphotos.com/1015337/5539/i/450/depositphotos_55398757-stock-photo-business-accounting.jpg',
      title: 'Accountant',
      experience: '1-2 years',
      education: 'Education in the field of economics',
      text: "Are you a detail-oriented individual with a knack for numbers? Join our team at the English Language Academy as an Accountant! We're currently seeking a skilled professional to manage our financial operations with precision and accuracy. As an integral part of our team, you'll be responsible for handling accounts, budgeting, and financial reporting. If you have a strong financial background and a passion for supporting educational initiatives, we encourage you to apply and help us maintain fiscal stability while fostering academic excellence.",
    },
    {
      id: 3,
      image:
        'https://media.istockphoto.com/id/1401460590/photo/businessman-working-on-laptop-with-document-management-icon.jpg?s=612x612&w=0&k=20&c=o8Ci6F_YCWFlKE2Yr6A2wbDvrZRwSB3YssLakLkrFBo=',
      title: 'IT Manager',
      experience: '3-5 years',
      education: 'Technical education',
      text: "Are you a tech-savvy problem solver with a passion for supporting educational technology? Here's your chance to shine! The English Language Academy is looking for an IT Specialist to join our dedicated team. As our IT expert, you'll be responsible for maintaining our digital infrastructure, troubleshooting technical issues, and implementing innovative solutions to enhance our teaching and learning environments. From managing software systems to ensuring network security, you'll play a vital role in keeping our academy running smoothly. If you're ready to use your technical skills to support our educational mission, apply now and be part of our exciting journey!",
    },
  ];

  ngAfterViewInit() {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          entry.target.classList.add('visible');
        } else {
          entry.target.classList.remove('visible');
        }
      });
    });

    document.querySelectorAll('.animate-on-scroll').forEach((element) => {
      observer.observe(element);
    });
  }
}
