import { DICTIONARY_API } from '../../../environments/environment.development';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DictionaryService {
  constructor(private http: HttpClient) {}
  searchDictionary(word: string): Observable<string> {
    return this.http.get<string>(DICTIONARY_API + word);
  }
}
