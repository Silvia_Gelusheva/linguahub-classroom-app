import { Component } from '@angular/core';
import { DictionaryService } from './dictionary.service';

@Component({
  selector: 'app-dictionary',
  templateUrl: './dictionary.component.html',
  styleUrls: ['./dictionary.component.css'],
})
export class DictionaryComponent {
  searchTerm: string = '';
  dictionaryData: any | undefined;

  constructor(private dictionaryService: DictionaryService) {}

  search() {
    if (!this.searchTerm.trim()) {
      return;
    }

    this.dictionaryService.searchDictionary(this.searchTerm).subscribe({
      next: (data) => {
        // console.log(data);
        this.dictionaryData = data;
      },
      error: (error) => {
        //TODO handle error with toast
        console.error(error.message);
      },
    });
  }
}
