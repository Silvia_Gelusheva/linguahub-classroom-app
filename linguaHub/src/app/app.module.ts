import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AboutComponent } from './components/about/about.component';
import { AdultsComponent } from './components/programmes/adults/adults.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { AppComponent } from './app.component';
import { AppInterceptorService } from './app.interceptor.service';
import { AppRoutingModule } from './app-routing.module';
import { BigCardComponent } from './elements/big-card/big-card.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CardComponent } from './components/programmes/card/card.component';
import { CareersComponent } from './components/about/careers/careers.component';
import { CoreModule } from './core/core.module';
import { CorporateComponent } from './components/programmes/corporate/corporate.component';
import { DictionaryComponent } from './components/dictionary/dictionary.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { GeneralComponent } from './components/about/general/general.component';
import { HomeComponent } from './components/home/home.component';
import { KidsComponent } from './components/programmes/kids/kids.component';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { NgModule } from '@angular/core';
import { ProgrammesComponent } from './components/programmes/programmes.component';
import { SharedModule } from './shared/shared.module';
import { SmallCardComponent } from './elements/small-card/small-card.component';
import { TeamComponent } from './components/about/team/team.component';
import { ToastComponent } from './shared/toast/toast.component';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    DictionaryComponent,
    AboutComponent,
    ProgrammesComponent,
    HomeComponent,
    KidsComponent,
    AdultsComponent,
    CorporateComponent,
    CardComponent,

    BigCardComponent,
    SmallCardComponent,
    GeneralComponent,
    TeamComponent,
    CareersComponent,
  ],
  imports: [
    BrowserModule,
    CoreModule,
    SharedModule,
    MatCardModule,
    MatIconModule,
    MatGridListModule,
    MatToolbarModule,
    MatFormFieldModule,
    FlexLayoutModule,
    HttpClientModule,
    AngularEditorModule,
    FormsModule,
    BrowserAnimationsModule,

    ToastrModule.forRoot({
      toastComponent: ToastComponent,
    }),
    AppRoutingModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
