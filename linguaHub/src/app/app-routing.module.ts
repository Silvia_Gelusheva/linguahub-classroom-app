import { RouterModule, Routes } from '@angular/router';

import { AboutComponent } from './components/about/about.component';
import { AdultsComponent } from './components/programmes/adults/adults.component';
import { CareersComponent } from './components/about/careers/careers.component';
import { CorporateComponent } from './components/programmes/corporate/corporate.component';
import { DictionaryComponent } from './components/dictionary/dictionary.component';
import { GeneralComponent } from './components/about/general/general.component';
import { HomeComponent } from './components/home/home.component';
import { KidsComponent } from './components/programmes/kids/kids.component';
import { NgModule } from '@angular/core';
import { NotFoundComponent } from './shared/not-found/not-found.component';
import { ProgrammesComponent } from './components/programmes/programmes.component';
import { TeamComponent } from './components/about/team/team.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'about',
    component: AboutComponent,
    children: [
      { path: 'general', component: GeneralComponent },
      { path: 'team', component: TeamComponent },
      { path: 'careers', component: CareersComponent },
      { path: '', redirectTo: 'General', pathMatch: 'full' },
    ],
  },
  {
    path: 'programmes',
    component: ProgrammesComponent,
    children: [
      { path: 'kids', component: KidsComponent },
      { path: 'adults', component: AdultsComponent },
      { path: 'corporate', component: CorporateComponent },
      { path: '', redirectTo: 'kids', pathMatch: 'full' },
    ],
  },
  { path: 'dictionary', component: DictionaryComponent },
  { path: 'not-found', component: NotFoundComponent },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('./dashboard-admin/dashboard-admin.module').then(
        (m) => m.DashboardAdminModule
      ),
  },
  {
    path: 'classroom',
    loadChildren: () =>
      import('./dashboard-course/dashboard-course.module').then(
        (m) => m.DashboardCourseModule
      ),
  },
  {
    path: 'auth',
    loadChildren: () => import('./user/user.module').then((m) => m.UserModule),
  },

  { path: '**', redirectTo: 'not-found' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
