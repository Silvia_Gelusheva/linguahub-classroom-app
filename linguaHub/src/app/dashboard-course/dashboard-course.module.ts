import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AngularEditorModule } from '@kolkov/angular-editor';
import { ClassroomComponent } from './classroom/classroom.component';
import { CommentComponent } from './classroom/theme/comment/comment.component';
import { CommonModule } from '@angular/common';
import { CoursesComponent } from './courses/courses.component';
import { DashboardCourseRoutingModule } from './dashboard-course-routing.module';
import { DashboardCoursesComponent } from './dashboard-courses/dashboard-courses.component';
import { KeysLengthPipe } from '../pipes/keysPipe';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { NgModule } from '@angular/core';
import { StudentsComponent } from './students/students.component';
import { ThemeComponent } from './classroom/theme/theme.component';

@NgModule({
  declarations: [
    StudentsComponent,
    ClassroomComponent,
    CoursesComponent,
    DashboardCoursesComponent,
    KeysLengthPipe,
    ThemeComponent,
    CommentComponent,
  ],
  imports: [
    CommonModule,
    DashboardCourseRoutingModule,
    AngularEditorModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    MatDialogModule,
    MatSidenavModule,
    MatIconModule,
  ],
})
export class DashboardCourseModule {}
