import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ActivatedRoute } from '@angular/router';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { CourseData } from 'src/app/types/CourseData';
import { DashboardAdminService } from 'src/app/dashboard-admin/dashboard-admin.service';
import { DashboardCourseService } from '../dashboard-course.service';
import { ThemeData } from 'src/app/types/ThemeData';
import { User } from 'src/app/types/User';
import { UserData } from 'src/app/types/UserData';
import { UserService } from '../../user/user.service';

@Component({
  selector: 'app-classroom',
  templateUrl: './classroom.component.html',
  styleUrls: ['./classroom.component.css'],
})
export class ClassroomComponent implements OnInit {
  course: CourseData | undefined;
  courseId: string | null = null;
  user: UserData | undefined;
  userName: string = '';
  themes: ThemeData[] = [];
  editMode: boolean = false;
  // editModeComment: boolean = false;
  themeForm: FormGroup;
  showThemeInput: boolean = false;

  currentDate: Date;
  selectedThemeId: string = '';
  studentsIds: string[] | undefined;
  students: UserData[] = [];
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [['bold']],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText',
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
  };
  constructor(
    private courseService: DashboardCourseService,
    private adminService: DashboardAdminService,
    private userService: UserService,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.themeForm = this.fb.group({
      lesson: ['', [Validators.required]],
      title: ['', [Validators.required, Validators.minLength(2)]],
      text: ['', [Validators.required, Validators.minLength(7)]],
      homework: ['', [Validators.required, Validators.minLength(2)]],
    });

    this.currentDate = new Date();
  }

  ngOnInit(): void {
    this.courseId = this.route.snapshot.paramMap.get('id');

    if (this.courseId) {
      this.courseService.getCourseById(this.courseId).subscribe({
        next: (res) => {
          this.course = res;
        },
        error: (error) => {
          console.error('Error fetching course:', error);
        },
      });
      this.fetchCourseThemes(this.courseId);
    }

    this.userService.user.subscribe((user: User | null) => {
      if (user) {
        this.userService.getUserById(user.id).subscribe({
          next: (fetchedUser) => {
            this.user = fetchedUser;
            this.userName = `${this.user.firstName} ${this.user.lastName}`;
          },
          error: (err) => {
            console.log(err.message);
          },
        });
      }
    });

    if (!this.courseId) return;

    this.adminService.getCourseById(this.courseId).subscribe({
      next: (response) => {
        this.course = response;
        this.studentsIds = response.students;

        if (this.studentsIds) {
          this.adminService.getUsersByIds(this.studentsIds).subscribe({
            next: (response) => {
              this.students = response;
            },
          });
        }
      },
    });
  }

  onThemeSelected(themeId: string) {
    this.selectedThemeId = themeId;
  }

  toggleThemeInput() {
    this.showThemeInput = !this.showThemeInput;
  }

  //! FETCH DATA

  fetchCourseThemes(courseId: string) {
    this.courseService.getThemes(courseId).subscribe({
      next: (themes) => {
        if (themes) {
          this.themes = Object.values(themes);
        } else {
          this.themes = [];
        }
      },
      error: (err) => {
        console.log(err.message);
      },
    });
  }

  //! THEME SECTION

  //* ADD THEME
  addNewTheme(themeForm: FormGroup) {
    if (themeForm.invalid || !this.courseId || !this.course) return;

    const newTheme: ThemeData = {
      id: '',
      lesson: this.themeForm.value.lesson,
      title: this.themeForm.value.title,
      text: this.themeForm.value.text,
      homework: this.themeForm.value.homework,
      author: this.userName,
      addedOn: new Date(),
      comments: {},
    };

    this.courseService
      .addThemeToCourseMessages(this.courseId, newTheme)
      .subscribe({
        next: () => {
          this.fetchCourseThemes(this.courseId!);
          themeForm.reset();
          this.toggleThemeInput();
        },
        error: (err) => {
          console.error('Error adding theme:', err.message);
        },
      });
  }
}
