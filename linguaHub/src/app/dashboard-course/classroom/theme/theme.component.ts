import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AngularEditorConfig } from '@kolkov/angular-editor';
import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';
import { CourseData } from 'src/app/types/CourseData';
import { DashboardCourseService } from '../../dashboard-course.service';
import { MatDialog } from '@angular/material/dialog';
import { ThemeData } from 'src/app/types/ThemeData';
import { User } from 'src/app/types/User';
import { UserData } from 'src/app/types/UserData';
import { UserService } from 'src/app/user/user.service';
import { editorConfig } from './editor-config';

@Component({
  selector: 'app-theme',
  templateUrl: './theme.component.html',
  styleUrls: ['./theme.component.css'],
})
export class ThemeComponent implements OnInit {
  themeId = this.route.snapshot.paramMap.get('themeId');
  courseId = this.route.snapshot.paramMap.get('id');
  course: CourseData | undefined;
  user: UserData | undefined;
  userName: string = '';
  theme: ThemeData | undefined;
  editMode: boolean = false;
  // editModeComment: boolean = false;
  themeForm: FormGroup;
  editForm: FormGroup;
  showThemeInput: boolean = false;
  showComments: boolean = false;
  currentDate: Date;
  editorConfig: AngularEditorConfig = editorConfig;

  constructor(
    private courseService: DashboardCourseService,
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private dialog: MatDialog
  ) {
    this.themeForm = this.fb.group({
      text: ['', [Validators.required, Validators.minLength(7)]],
    });
    this.editForm = this.fb.group({
      lesson: ['', [Validators.required]],
      title: ['', [Validators.required, Validators.minLength(2)]],
      text: ['', [Validators.required, Validators.minLength(7)]],
      homework: ['', [Validators.required, Validators.minLength(2)]],
    });

    this.currentDate = new Date();
  }

  ngOnInit(): void {
    if (this.courseId) {
      this.courseService.getCourseById(this.courseId).subscribe({
        next: (res) => {
          this.course = res;
        },
        error: (error) => {
          console.error('Error fetching course:', error);
        },
      });
    }

    this.userService.user.subscribe((user: User | null) => {
      if (user) {
        this.userService.getUserById(user.id).subscribe({
          next: (fetchedUser) => {
            this.user = fetchedUser;
            this.userName = `${this.user.firstName} ${this.user.lastName}`;
          },
          error: (err) => {
            console.log(err.message);
          },
        });
      }
    });

    if (this.courseId && this.themeId) {
      this.fetchTheme(this.courseId, this.themeId);
    } else {
      console.error('courseId or themeId is null');
    }
  }

  toggleComments() {
    this.showComments = !this.showComments;
  }
  onCommentAdded() {
    this.fetchTheme(this.courseId!, this.themeId!);
  }

  onCommentDeleted() {
    this.fetchTheme(this.courseId!, this.themeId!);
  }

  //! FETCH THEME
  fetchTheme(courseId: string, themeId: string) {
    this.courseService.getThemeById(courseId, themeId).subscribe({
      next: (theme) => {
        if (theme) {
          this.theme = theme;
        } else {
          console.log('Error: theme is undefined');
        }
      },
      error: (err) => {
        console.error('Error fetching theme:', err.message);
      },
    });
  }

  //! THEME SECTION

  //* TOGGLE THEME
  onToggle(theme: ThemeData) {
    this.editForm.patchValue({
      lesson: theme.lesson,
      title: theme.title,
      text: theme.text,
      homework: theme.homework,
    });

    theme.editMode = !theme.editMode;
  }

  //* UPDATE THEME
  editTheme(editForm: FormGroup, theme: ThemeData) {
    const newText = editForm.value.text;
    const newLesson = editForm.value.lesson;
    const newTitle = editForm.value.title;
    const newHomework = editForm.value.homework;

    if (!newText.trim() || !this.courseId) {
      return;
    }

    theme.text = newText;
    theme.lesson = newLesson;
    theme.title = newTitle;
    theme.homework = newHomework;

    theme.editMode = false;

    this.courseService
      .updateTheme(
        theme.id!,
        theme.lesson,
        theme.title,
        theme.text,
        theme.homework,
        theme.author,
        theme.addedOn,
        theme.comments,
        this.courseId
      )
      .subscribe({
        next: () => {
          this.showThemeInput = !this.showThemeInput;
        },
      });
  }

  //* DELETE THEME
  deleteTheme(theme: ThemeData) {
    if (!theme || !theme.id || !this.courseId) {
      console.log('theme del issue', theme, this.courseId);
      return;
    }

    this.courseService.deleteTheme(this.courseId, theme.id!).subscribe({
      next: () => {
        this.router.navigate([`/classroom/courses/${this.courseId}`]);
      },
      error: (error) => {
        console.error('Error deleting theme:', error);
      },
    });
  }

  confirmDelete(theme: ThemeData): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      data: { message: 'Are you sure you want to delete this theme?' },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.deleteTheme(theme);
      }
    });
  }
}
