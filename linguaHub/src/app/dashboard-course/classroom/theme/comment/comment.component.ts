import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { CommentData } from 'src/app/types/CommentData';
import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';
import { DashboardCourseService } from 'src/app/dashboard-course/dashboard-course.service';
import { MatDialog } from '@angular/material/dialog';
import { ThemeData } from 'src/app/types/ThemeData';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css'],
})
export class CommentComponent {
  @Input() userName: string = '';
  @Input() theme!: ThemeData;
  @Input() courseId: string | null = null;
  @Input() themeId: string | null = null;
  @Input() showComments: boolean = false;

  @Output() commentAdded = new EventEmitter<void>();
  @Output() commentDeleted = new EventEmitter<void>();

  commentForm: FormGroup;
  editCommentForm: FormGroup;
  currentDate: Date = new Date();

  constructor(
    private courseService: DashboardCourseService,
    private fb: FormBuilder,
    private dialog: MatDialog
  ) {
    this.commentForm = this.fb.group({
      text: ['', [Validators.required, Validators.minLength(2)]],
    });
    this.editCommentForm = this.fb.group({
      text: ['', [Validators.required, Validators.minLength(2)]],
    });
  }

  //!  ADD COMMENT
  postComment(commentForm: FormGroup, theme: ThemeData) {
    if (commentForm.invalid || !this.courseId || !this.themeId) {
      return;
    }

    const newComment: CommentData = {
      text: commentForm.value.text,
      author: this.userName,
      addedOn: new Date(),
    };

    this.courseService
      .addCommentToTheme(this.courseId!, theme.id!, newComment)
      .subscribe({
        next: () => {
          this.commentAdded.emit();
        },
        error: (err) => {
          console.error('Error adding comment:', err);
        },
      });
    commentForm.reset();
  }

  //!  EDIT COMMENT
  onToggleComment(comment: CommentData) {
    this.editCommentForm.patchValue({
      text: comment.text,
    });

    comment.editModeComment = !comment.editModeComment;
  }

  editComment(
    editCommentForm: FormGroup,
    theme: ThemeData,
    comment: CommentData
  ) {
    if (editCommentForm.invalid) {
      console.error('Invalid form');
      return;
    }

    const newText = editCommentForm.get('text')?.value.trim();
    if (!newText) {
      console.error('Text cannot be empty');
      return;
    }

    if (!this.courseId) {
      console.error('CourseId is undefined');
      return;
    }

    comment.text = newText;
    comment.editModeComment = false;

    this.courseService
      .updateComment(this.courseId, theme.id!, comment.id!, comment)
      .subscribe({
        next: () => {
          console.log('Comment updated successfully');
        },
        error: (err) => {
          console.error('Error updating comment:', err);
        },
      });
  }

  //!  DELETE COMMENT

  deleteComment(theme: ThemeData, comment: CommentData) {
    if (!comment || !theme || !this.courseId) {
      console.error('Error: Invalid parameters for deleting comment');
      return;
    }

    this.courseService
      .deleteTCommentForTheme(this.courseId!, theme.id!, comment.id!)
      .subscribe({
        next: () => {
          this.commentDeleted.emit();
        },
        error: (err) => {
          console.error('Error deleting comment:', err);
        },
      });
  }

  confirmDeleteComment(theme: ThemeData, comment: CommentData) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
      data: { message: 'Are you sure you want to delete this comment?' },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.deleteComment(theme, comment);
      }
    });
  }
}
