import { Observable, map, switchMap } from 'rxjs';

import { CommentData } from '../types/CommentData';
import { CourseData } from '../types/CourseData';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { REALTIME_URL } from 'src/environments/environment.development';
import { ThemeData } from '../types/ThemeData';
import { UserData } from '../types/UserData';

@Injectable({
  providedIn: 'root',
})
export class DashboardCourseService {
  constructor(private http: HttpClient) {}

  //! USER REQUESTS

  // fetchUsers(): Observable<UserData[]> {
  //   return this.http
  //     .get<{ [key: string]: any }>(`${REALTIME_URL}/users.json`)
  //     .pipe(
  //       map((response) => {
  //         let users = [];
  //         for (let key in response) {
  //           if (response.hasOwnProperty(key)) {
  //             const user = { ...response[key], id: key };
  //             users.push(user);
  //           }
  //         }
  //         return users;
  //       })
  //     );
  // }

  //! COURSES REQUESTS

  getCourses(): Observable<CourseData[]> {
    return this.http
      .get<{ [key: string]: any }>(`${REALTIME_URL}/courses.json`)
      .pipe(
        map((response) => {
          let courses: CourseData[] = [];
          for (let key in response) {
            if (response.hasOwnProperty(key)) {
              courses.push({ ...response[key], id: key });
            }
          }
          return courses;
        })
      );
  }

  getCourseById(id: string | null): Observable<CourseData | undefined> {
    return this.http.get<CourseData | undefined>(
      `${REALTIME_URL}/courses/${id}.json`
    );
  }

  //! THEMES REQUESTS / CRUD

  addThemeToCourseMessages(courseId: string, theme: ThemeData) {
    const key = this.http.post<{ name: string }>(
      `${REALTIME_URL}/messages/${courseId}.json`,
      theme
    );
    return key.pipe(
      switchMap((response) => {
        const themeWithKey: ThemeData = {
          ...theme,
          id: response.name,
        };
        return this.http.put(
          `${REALTIME_URL}/messages/${courseId}/${response.name}.json`,
          themeWithKey
        );
      })
    );
  }

  getThemes(courseId: string | null): Observable<ThemeData | undefined> {
    return this.http.get<ThemeData | undefined>(
      `${REALTIME_URL}/messages/${courseId}.json`
    );
  }
  getThemeById(
    courseId: string | null,
    themeId: string | null
  ): Observable<ThemeData | undefined> {
    return this.http.get<ThemeData | undefined>(
      `${REALTIME_URL}/messages/${courseId}/${themeId}.json`
    );
  }
  updateTheme(
    id: string,
    lesson: number,
    title: string,
    text: string,
    homework: string,
    author: string,
    addedOn: Date,
    comments: { [commentId: string]: CommentData },
    courseId: string
  ) {
    return this.http.put(`${REALTIME_URL}/messages/${courseId}/${id}.json`, {
      id,
      lesson,
      title,
      text,
      homework,
      author,
      addedOn,
      comments,
    });
  }

  deleteTheme(courseId: string, themeId: string): Observable<ThemeData> {
    return this.http.delete<ThemeData>(
      `${REALTIME_URL}/messages/${courseId}/${themeId}.json`
    );
  }

  //! COMMENTS REQUESTS / CRUD

  addCommentToTheme(courseId: string, themeId: string, comment: CommentData) {
    const key = this.http.post<{ name: string }>(
      `${REALTIME_URL}/messages/${courseId}/${themeId}/comments.json`,
      comment
    );

    return key.pipe(
      switchMap((response) => {
        const commentWithKey: CommentData = {
          ...comment,
          id: response.name,
        };
        return this.http.put(
          `${REALTIME_URL}/messages/${courseId}/${themeId}/comments/${response.name}.json`,
          commentWithKey
        );
      })
    );
  }

  getCommentsForTheme(
    courseId: string | null,
    themeId: string
  ): Observable<CommentData[] | undefined> {
    return this.http.get<CommentData[] | undefined>(
      `${REALTIME_URL}/messages/${courseId}/${themeId}/comments.json`
    );
  }

  updateComment(
    courseId: string,
    themeId: string,
    commentId: string,
    updatedComment: CommentData
  ): Observable<any> {
    return this.http.put(
      `${REALTIME_URL}/messages/${courseId}/${themeId}/comments/${commentId}.json`,
      updatedComment
    );
  }

  deleteTCommentForTheme(
    courseId: string,
    themeId: string,
    commentId: string
  ): Observable<CommentData> {
    return this.http.delete<CommentData>(
      `${REALTIME_URL}/messages/${courseId}/${themeId}/comments/${commentId}.json`
    );
  }
}
