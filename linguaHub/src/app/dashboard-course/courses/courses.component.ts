import { Component, OnInit } from '@angular/core';

import { CourseData } from 'src/app/types/CourseData';
import { DashboardAdminService } from '../../dashboard-admin/dashboard-admin.service';
import { Router } from '@angular/router';
import { User } from 'src/app/types/User';
import { UserData } from 'src/app/types/UserData';
import { UserService } from 'src/app/user/user.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css'],
})
export class CoursesComponent implements OnInit {
  courses: CourseData[] = [];
  user: UserData | undefined;
  userId: string | undefined = '';

  constructor(
    private adminService: DashboardAdminService,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.userService.user.subscribe((user: User | null) => {
      if (user) {
        this.userService.getUserById(user.id).subscribe({
          next: (response) => {
            this.user = response;
            this.userId = this.user.id;
          },
        });
      }
    });

    this.adminService.getCourses('').subscribe({
      next: (response) => {
        this.courses = response;
      },
    });
  }

  isUserEnrolledOrTeacher(course: CourseData): boolean {
    if (!this.userId) return false;

    const isEnrolled = course.students?.includes(this.user?.id as never);
    const isTeacher =
      course?.teacher === `${this.user?.firstName} ${this.user?.lastName}`;

    return isEnrolled || isTeacher;
  }
  navigateToClassroom(courseId: string) {
    this.router.navigate(['/classroom/courses', courseId]);
  }
}
