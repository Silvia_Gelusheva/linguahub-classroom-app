import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../RouteGuards/authGuard';
import { ClassroomComponent } from './classroom/classroom.component';
import { CourseGuard } from '../RouteGuards/courseGuard';
import { CoursesComponent } from './courses/courses.component';
import { DashboardCoursesComponent } from './dashboard-courses/dashboard-courses.component';
import { NgModule } from '@angular/core';
import { StudentsComponent } from './students/students.component';
import { ThemeComponent } from './classroom/theme/theme.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardCoursesComponent,
    canActivate: [AuthGuard, CourseGuard],
    children: [
      { path: 'courses', component: CoursesComponent },
      { path: 'courses/:id', component: ClassroomComponent },
      { path: 'students', component: StudentsComponent },
      {
        path: 'courses/:id/:themeId',
        component: ThemeComponent,
      },
      { path: '', redirectTo: 'courses', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardCourseRoutingModule {}
