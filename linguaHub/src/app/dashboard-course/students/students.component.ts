import { Component, OnInit } from '@angular/core';

import { DashboardAdminService } from 'src/app/dashboard-admin/dashboard-admin.service';
import { DashboardCourseService } from '../dashboard-course.service';
import { Router } from '@angular/router';
import { UserData } from 'src/app/types/UserData';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css'],
})
export class StudentsComponent implements OnInit {
  students: UserData[] = [];
  courseName: string = '';

  constructor(
    private dashboardAdminService: DashboardAdminService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.dashboardAdminService.getUsers('').subscribe({
      next: (res: UserData[]) => {
        this.students = res.filter((student) => student.role === 3);
      },
      error: (error) => {
        console.error('Error fetching users:', error);
      },
    });
  }

  navigateToClassroom(courseId: string) {
    this.router.navigate(['/classroom/courses', courseId]);
  }
}
