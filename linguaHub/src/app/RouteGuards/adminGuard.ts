import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root',
})
export class AdminGuard implements CanActivate {
  constructor(private userService: UserService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> {
    return this.userService.user.pipe(
      take(1),
      switchMap((user) => {
        if (user && user.id) {
          return this.userService.isAdmin(user.id).pipe(
            map((isAdmin) => {
              if (isAdmin) {
                return true;
              } else {
                return this.router.createUrlTree(['/not-found']);
              }
            })
          );
        } else {
          return of(this.router.createUrlTree(['/not-found']));
        }
      })
    );
  }
}
