import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'keysLength',
})
export class KeysLengthPipe implements PipeTransform {
  transform(value: any): number {
    if (!value || typeof value !== 'object') {
      return 0;
    }
    return Object.keys(value).length;
  }
}
