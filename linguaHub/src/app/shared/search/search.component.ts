import { Component, EventEmitter, Output } from '@angular/core';

import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent {
  @Output() searchQuery = new EventEmitter<string>();
  searchForm = this.fb.group({
    searchValue: '',
  });

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.searchForm.get('searchValue')?.valueChanges.subscribe((value) => {
      if (value !== null) {
        this.searchQuery.emit(value.trim());
      }
    });
  }
}
