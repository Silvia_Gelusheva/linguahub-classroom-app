import { CommonModule } from '@angular/common';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { LoaderComponent } from './loader/loader.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { NgModule } from '@angular/core';
import { NotFoundComponent } from './not-found/not-found.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SearchComponent } from './search/search.component';
import { ToastComponent } from './toast/toast.component';
@NgModule({
  declarations: [
    LoaderComponent,
    NotFoundComponent,
    ToastComponent,
    SearchComponent,
    ConfirmDialogComponent,
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatDialogModule,
    RouterModule,
    ReactiveFormsModule,
  ],
  exports: [
    LoaderComponent,
    ToastComponent,
    NotFoundComponent,
    SearchComponent,
  ],
})
export class SharedModule {}
