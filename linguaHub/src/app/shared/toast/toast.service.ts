import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { Observable, Subject } from 'rxjs';

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ToastService {
  private toastSubject = new Subject<any>();

  constructor(private toastr: ToastrService) {}

  get toast$(): Observable<any> {
    return this.toastSubject.asObservable();
  }

  private emitToastEvent(type: string, message: string, title?: string) {
    this.toastSubject.next({ type, message, title });
  }

  success(message: string, title?: string, config?: Partial<IndividualConfig>) {
    this.toastr.success(message, title, config);
    this.emitToastEvent('success', message, title);
  }

  error(message: string, title?: string, config?: Partial<IndividualConfig>) {
    this.toastr.error(message, title, config);
    this.emitToastEvent('error', message, title);
  }

  // warning(message: string, title?: string, config?: Partial<IndividualConfig>) {
  //   this.toastr.warning(message, title, config);
  //   this.emitToastEvent('warning', message, title);
  // }

  // info(message: string, title?: string, config?: Partial<IndividualConfig>) {
  //   this.toastr.info(message, title, config);
  //   this.emitToastEvent('info', message, title);
  // }
}
