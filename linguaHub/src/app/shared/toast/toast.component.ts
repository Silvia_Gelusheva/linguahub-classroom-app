import { Component, OnDestroy } from '@angular/core';
import { Subscription, timer } from 'rxjs';

import { ToastService } from './toast.service';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-toaster',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.css'],
})
export class ToastComponent implements OnDestroy {
  errorMessage: string | null = null;
  successMessage: string | null = null;
  warningMessage: string | null = null;
  private subscription: Subscription;
  private toastTimer: Subscription | undefined;

  constructor(private toastService: ToastService) {
    this.subscription = this.toastService.toast$.subscribe(
      ({ type, message }) => {
        if (type === 'error' || type === 'success' || type === 'warning') {
          this.errorMessage = type === 'error' ? message : null;
          this.successMessage = type === 'success' ? message : null;
          this.warningMessage = type === 'warning' ? message : null;

          // Clear any existing timer
          if (this.toastTimer) {
            this.toastTimer.unsubscribe();
          }

          // Start a new timer to clear toast after 5 seconds
          this.toastTimer = timer(5000).subscribe(() => {
            this.dismiss();
          });
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    // Cleanup timer to prevent memory leaks
    if (this.toastTimer) {
      this.toastTimer.unsubscribe();
    }
  }

  dismiss() {
    this.errorMessage = null;
    this.successMessage = null;
    this.warningMessage = null;
    if (this.toastTimer) {
      this.toastTimer.unsubscribe();
    }
  }
}
